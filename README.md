# Sitio Web - Usuarios de Software Libre Jujuy

Este proyecto se ha creado para albergar el desarrollo del sitio web para el grupo **USL Jujuy**. Como propuesta se realizarlo usando el CMS [Wordpress](https://wordpress.org/)

## General: visión general del sitio

1. **Blog** usljujuy.com.ar como blog informativo de la comunidad, con entradas de miembros de la comunidad. Usar subdominios para actividades específicas. ejemplo: flisol.usljujuy.com.ar para la actividad referente al flisol.

2. **Landing** otro enfoque seria usar usljujuy.com.ar como landing page e información general de la comunidad, luego en blog.usljujuy.com.ar colocar el blog en si con las entradas y publicaciones de los miembros y flisol.usljujuy.com.ar para las actividades relacionadas al flisol o algun evento.

## Funcionalidades

1. **Sección de noticias:** Actividades, eventos o encuentros de la comunidad que se estén llevando a cabo para contribuir en la difusión del SL.
2. **Redes sociales:** Sección que muestre la actividad de usljujuy en twitter y facebook.
3. **Recursos:** Recursos disponibles que la comunidad pueda compartir (~~).
4. **Analitics:** Integrar el sitio una plataforma de analisis, para obntener informacion de la actividad del sitio. *Ejemplo* [Piwik](https://piwik.org/) bajo GPLv3
5. **Comentarios:** Integrar al sitio un sistema de comentarios.
6. **Contacto:** Formulario de contacto, links a telegram, IRC, etc.
7. **Links:** Enlaces de interés.
8. **Donaciones:**Pasarela de pago para recibir donaciones.
9. **Amigos:** Sección para otras comunidades/grupos. Difusión de blogs relacionados con tecnología.
10. **Sección Quiénes somos**
	* Historia
	* Código de conducta
	* Miembros
11. **Noticias:** Próximos eventos.

## Ideas (agreguen sus ideas y visiones)

* Creo que a la gente que tendríamos que dirigirnos es principalmente a los jóvenes de la secundaria, con tips que le ayuden a sacar el máximo provecho a su netbook.
* Cultura Libre, efocar tambien en atraer gente no informática.
* ...(escríbeme)

## Git Repository

[https://gitlab.com/usljujuy/usljujuysite]( https://gitlab.com/usljujuy/usljujuysite)

## Issues

[https://gitlab.com/usljujuy/usljujuysite/issues](https://gitlab.com/usljujuy/usljujuysite/issues)

## Wiki

[https://gitlab.com/usljujuy/usljujuysite/wikis](https://gitlab.com/usljujuy/usljujuysite/wikis)





