/* WP Hardening - ba43d79cf730cff7a87106df2834c69a630cbd75c71cf1226789cc25771c1687 */
(function ( tinymce ) {
	'use strict';

	tinymce.PluginManager.add( 'wpembed', function ( editor, url ) {
		editor.on( 'init', function () {
			var scriptId = editor.dom.uniqueId();

			var scriptElm = editor.dom.create( 'script', {
				id: scriptId,
				type: 'text/javascript',
				src: url + '/../../../wp-embed.js'
			} );

			editor.getDoc().getElementsByTagName( 'head' )[ 0 ].appendChild( scriptElm );
		} );
	} );
})( window.tinymce );
