/* WP Hardening - 807f86c0e50067c7d76c6754731333301a0d3422b3c3237f95748cfd3c321f5f */
/* global setPostThumbnailL10n, ajaxurl, post_id, alert */
/* exported WPSetAsThumbnail */

function WPSetAsThumbnail( id, nonce ) {
	var $link = jQuery('a#wp-post-thumbnail-' + id);

	$link.text( setPostThumbnailL10n.saving );
	jQuery.post(ajaxurl, {
		action: 'set-post-thumbnail', post_id: post_id, thumbnail_id: id, _ajax_nonce: nonce, cookie: encodeURIComponent( document.cookie )
	}, function(str){
		var win = window.dialogArguments || opener || parent || top;
		$link.text( setPostThumbnailL10n.setThumbnail );
		if ( str == '0' ) {
			alert( setPostThumbnailL10n.error );
		} else {
			jQuery('a.wp-post-thumbnail').show();
			$link.text( setPostThumbnailL10n.done );
			$link.fadeOut( 2000 );
			win.WPSetThumbnailID(id);
			win.WPSetThumbnailHTML(str);
		}
	}
	);
}
